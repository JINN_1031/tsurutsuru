<?php
/*
Template Name: 無料カウンセリング（共有用）
*/
?>
<?php get_header(); ?>

<section class="counseling_calender">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>無料カウンセリング<span class="title_dec-r"></span></h2>
    <!-- *****リボンがまだ↓ -->
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/counseling/dec_counseling_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>
  <h2>第3希望日まで選択してください</h2>
  <div class="cal-view_container"></div>

  <h2>無料カウンセリング申し込み</h2>

  <div class="form_table">
    <div role="form" class="wpcf7" id="wpcf7-f100-o1" lang="ja" dir="ltr">
      <div class="screen-reader-response"></div>
      <form action="/counseling/#wpcf7-f100-o1" method="post" class="wpcf7-form" novalidate="novalidate">
        <div style="display: none;">
          <input type="hidden" name="_wpcf7" value="100" />
          <input type="hidden" name="_wpcf7_version" value="5.1.4" />
          <input type="hidden" name="_wpcf7_locale" value="ja" />
          <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f100-o1" />
          <input type="hidden" name="_wpcf7_container_post" value="0" />
        </div>

        <table>
          <tr>
            <th><span class="any">任意</span>来店希望日時</th>
            <td>
              <span class="wpcf7-form-control-wrap visit-date1"><input type="text" name="visit-date1" value="" size="40" class="wpcf7-form-control wpcf7-text visit-date" id="date1" aria-invalid="false" /></span><br><span class="wpcf7-form-control-wrap visit-date2"><input type="text" name="visit-date2" value="" size="40" class="wpcf7-form-control wpcf7-text visit-date" id="date2" aria-invalid="false" /></span><br><span class="wpcf7-form-control-wrap visit-date3"><input type="text" name="visit-date3" value="" size="40" class="wpcf7-form-control wpcf7-text visit-date" id="date3" aria-invalid="false" /></span>
            </td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>お名前</th>
            <td><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="例）脱毛 花子" /></span></td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>フリガナ</th>
            <td><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="例）ダツモウ ハナコ" /></span></td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>生年月日</th>
            <td><span class="wpcf7-form-control-wrap birth-y"><select name="birth-y" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required birth-y" id="birth-y" aria-required="true" aria-invalid="false">
                  <option value="--">--</option>
                  <option value="1919">1919</option>
                  <option value="1920">1920</option>
                  <option value="1921">1921</option>
                  <option value="1922">1922</option>
                  <option value="1923">1923</option>
                  <option value="1924">1924</option>
                  <option value="1925">1925</option>
                  <option value="1926">1926</option>
                  <option value="1927">1927</option>
                  <option value="1928">1928</option>
                  <option value="1929">1929</option>
                  <option value="1930">1930</option>
                  <option value="1931">1931</option>
                  <option value="1932">1932</option>
                  <option value="1933">1933</option>
                  <option value="1934">1934</option>
                  <option value="1935">1935</option>
                  <option value="1936">1936</option>
                  <option value="1937">1937</option>
                  <option value="1938">1938</option>
                  <option value="1939">1939</option>
                  <option value="1940">1940</option>
                  <option value="1941">1941</option>
                  <option value="1942">1942</option>
                  <option value="1943">1943</option>
                  <option value="1944">1944</option>
                  <option value="1945">1945</option>
                  <option value="1946">1946</option>
                  <option value="1947">1947</option>
                  <option value="1948">1948</option>
                  <option value="1949">1949</option>
                  <option value="1950">1950</option>
                  <option value="1951">1951</option>
                  <option value="1952">1952</option>
                  <option value="1953">1953</option>
                  <option value="1954">1954</option>
                  <option value="1955">1955</option>
                  <option value="1956">1956</option>
                  <option value="1957">1957</option>
                  <option value="1958">1958</option>
                  <option value="1959">1959</option>
                  <option value="1960">1960</option>
                  <option value="1961">1961</option>
                  <option value="1962">1962</option>
                  <option value="1963">1963</option>
                  <option value="1964">1964</option>
                  <option value="1965">1965</option>
                  <option value="1966">1966</option>
                  <option value="1967">1967</option>
                  <option value="1968">1968</option>
                  <option value="1969">1969</option>
                  <option value="1970">1970</option>
                  <option value="1971">1971</option>
                  <option value="1972">1972</option>
                  <option value="1973">1973</option>
                  <option value="1974">1974</option>
                  <option value="1975">1975</option>
                  <option value="1976">1976</option>
                  <option value="1977">1977</option>
                  <option value="1978">1978</option>
                  <option value="1979">1979</option>
                  <option value="1980">1980</option>
                  <option value="1981">1981</option>
                  <option value="1982">1982</option>
                  <option value="1983">1983</option>
                  <option value="1984">1984</option>
                  <option value="1985">1985</option>
                  <option value="1986">1986</option>
                  <option value="1987">1987</option>
                  <option value="1988">1988</option>
                  <option value="1989">1989</option>
                  <option value="1990">1990</option>
                  <option value="1991">1991</option>
                  <option value="1992">1992</option>
                  <option value="1993">1993</option>
                  <option value="1994">1994</option>
                  <option value="1995">1995</option>
                  <option value="1996">1996</option>
                  <option value="1997">1997</option>
                  <option value="1998">1998</option>
                  <option value="1999">1999</option>
                  <option value="2000">2000</option>
                  <option value="2001">2001</option>
                  <option value="2002">2002</option>
                  <option value="2003">2003</option>
                  <option value="2004">2004</option>
                  <option value="2005">2005</option>
                  <option value="2006">2006</option>
                  <option value="2007">2007</option>
                  <option value="2008">2008</option>
                  <option value="2009">2009</option>
                  <option value="2010">2010</option>
                  <option value="2011">2011</option>
                  <option value="2012">2012</option>
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
                  <option value="2016">2016</option>
                  <option value="2017">2017</option>
                  <option value="2018">2018</option>
                  <option value="2019">2019</option>
                </select></span>年<span class="wpcf7-form-control-wrap birth-m"><select name="birth-m" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required birth-m" id="birth-m" aria-required="true" aria-invalid="false">
                  <option value="--">--</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select></span>月<span class="wpcf7-form-control-wrap birth-d"><select name="birth-d" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required birth-d" id="birth-d" aria-required="true" aria-invalid="false">
                  <option value="--">--</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                  <option value="24">24</option>
                  <option value="25">25</option>
                  <option value="26">26</option>
                  <option value="27">27</option>
                  <option value="28">28</option>
                  <option value="29">29</option>
                  <option value="30">30</option>
                  <option value="31">31</option>
                </select></span>日</td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>年齢</th>
            <td><span class="wpcf7-form-control-wrap age"><select name="age" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required birth-d" id="birth-d" aria-required="true" aria-invalid="false">
                  <option value="--">--</option>
                  <option value="17歳以下">17歳以下</option>
                  <option value="18歳">18歳</option>
                  <option value="19歳">19歳</option>
                  <option value="20歳">20歳</option>
                  <option value="21歳">21歳</option>
                  <option value="22歳">22歳</option>
                  <option value="23歳">23歳</option>
                  <option value="24歳">24歳</option>
                  <option value="25歳">25歳</option>
                  <option value="26歳">26歳</option>
                  <option value="27歳">27歳</option>
                  <option value="28歳">28歳</option>
                  <option value="29歳">29歳</option>
                  <option value="30歳">30歳</option>
                  <option value="31歳">31歳</option>
                  <option value="32歳">32歳</option>
                  <option value="33歳">33歳</option>
                  <option value="34歳">34歳</option>
                  <option value="35歳">35歳</option>
                  <option value="36歳">36歳</option>
                  <option value="37歳">37歳</option>
                  <option value="38歳">38歳</option>
                  <option value="39歳">39歳</option>
                  <option value="40歳">40歳</option>
                  <option value="41歳">41歳</option>
                  <option value="42歳">42歳</option>
                  <option value="43歳">43歳</option>
                  <option value="44歳">44歳</option>
                  <option value="45歳">45歳</option>
                  <option value="46歳">46歳</option>
                  <option value="47歳">47歳</option>
                  <option value="48歳">48歳</option>
                  <option value="49歳">49歳</option>
                  <option value="50歳">50歳</option>
                  <option value="51歳">51歳</option>
                  <option value="52歳">52歳</option>
                  <option value="53歳">53歳</option>
                  <option value="54歳">54歳</option>
                  <option value="55歳">55歳</option>
                  <option value="56歳">56歳</option>
                  <option value="57歳">57歳</option>
                  <option value="58歳">58歳</option>
                  <option value="59歳">59歳</option>
                  <option value="60歳">60歳</option>
                  <option value="61歳">61歳</option>
                  <option value="62歳">62歳</option>
                  <option value="63歳">63歳</option>
                  <option value="64歳">64歳</option>
                  <option value="65歳以上">65歳以上</option>
                </select></span></td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>脱毛希望部位</th>
            <td><span class="wpcf7-form-control-wrap desire-part"><span class="wpcf7-form-control wpcf7-radio desire-part" id="desire-part"><span class="wpcf7-list-item first"><input type="radio" name="desire-part" value="全身" checked="checked" /><span class="wpcf7-list-item-label">全身</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="顔" /><span class="wpcf7-list-item-label">顔</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="おでこ" /><span class="wpcf7-list-item-label">おでこ</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="眉毛" /><span class="wpcf7-list-item-label">眉毛</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="うなじ・襟足" /><span class="wpcf7-list-item-label">うなじ・襟足</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="鼻" /><span class="wpcf7-list-item-label">鼻</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="鼻毛" /><span class="wpcf7-list-item-label">鼻毛</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="もみあげ" /><span class="wpcf7-list-item-label">もみあげ</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="あご・ひげ" /><span class="wpcf7-list-item-label">あご・ひげ</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="ワキ" /><span class="wpcf7-list-item-label">ワキ</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="胸" /><span class="wpcf7-list-item-label">胸</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="乳首・乳輪" /><span class="wpcf7-list-item-label">乳首・乳輪</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="腕" /><span class="wpcf7-list-item-label">腕</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="手・指" /><span class="wpcf7-list-item-label">手・指</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="背中" /><span class="wpcf7-list-item-label">背中</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="お腹" /><span class="wpcf7-list-item-label">お腹</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="VIO" /><span class="wpcf7-list-item-label">VIO</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="Vライン" /><span class="wpcf7-list-item-label">Vライン</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="Iライン" /><span class="wpcf7-list-item-label">Iライン</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="Oライン" /><span class="wpcf7-list-item-label">Oライン</span></span><span class="wpcf7-list-item"><input type="radio" name="desire-part" value="お尻" /><span class="wpcf7-list-item-label">お尻</span></span><span class="wpcf7-list-item last"><input type="radio" name="desire-part" value="足・ひざ下" /><span class="wpcf7-list-item-label">足・ひざ下</span></span>
                </span>
              </span>
            </td>
          </tr>
          <tr>
            <th><span class="imp">必須</span>電話番号</th>
            <td><span class="wpcf7-form-control-wrap tel-num"><input type="tel" name="tel-num" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel tel-num" id="tel-num" aria-required="true" aria-invalid="false" placeholder="例）03-1234-5678" /></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>住所</th>
            <td><span class="wpcf7-form-control-wrap address"><input type="text" name="address" value="" size="40" class="wpcf7-form-control wpcf7-text address" id="address" aria-invalid="false" /></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>利用人数</th>
            <td><span class="wpcf7-form-control-wrap number"><select name="number" class="wpcf7-form-control wpcf7-select p-num" id="p-num" aria-invalid="false">
                  <option value="1名">1名</option>
                  <option value="2名">2名</option>
                  <option value="3名以上">3名以上</option>
                </select></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>当日の施術を希望されますか？</th>
            <td><span class="wpcf7-form-control-wrap treatment"><select name="treatment" class="wpcf7-form-control wpcf7-select treatment" id="treatment" aria-invalid="false">
                  <option value="はい">はい</option>
                  <option value="いいえ">いいえ</option>
                </select></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>脱毛経験はございますか？</th>
            <td><span class="wpcf7-form-control-wrap exp"><select name="exp" class="wpcf7-form-control wpcf7-select exp" id="exp" aria-invalid="false">
                  <option value="はい">はい</option>
                  <option value="いいえ">いいえ</option>
                </select></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>キャンペーンの通知を希望しますか？</th>
            <td><span class="wpcf7-form-control-wrap notification"><select name="notification" class="wpcf7-form-control wpcf7-select notification" id="notification" aria-invalid="false">
                  <option value="はい">はい</option>
                  <option value="いいえ">いいえ</option>
                </select></span></td>
          </tr>
          <tr>
            <th><span class="any">任意</span>質問など</th>
            <td><span class="wpcf7-form-control-wrap ask"><textarea name="ask" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea ask" id="ask" aria-invalid="false" placeholder="例）料金について"></textarea></span></td>
          </tr>
        </table>
        <p><input type="submit" value="無料カウンセリングを申し込む" class="wpcf7-form-control wpcf7-submit bg-blue" /></p>
        <div class="wpcf7-response-output wpcf7-display-none"></div>
      </form>
    </div>
  </div>
</section>
<?php
get_footer();
