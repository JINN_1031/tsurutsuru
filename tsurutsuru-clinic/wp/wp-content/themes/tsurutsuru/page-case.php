<?php get_header(); ?>

<div class="bg-lowsky">
  <section class="case_wrap">
    <div class="title-wrap">
      <h2><span class="title_dec-l"></span>体験事例<span class="title_dec-r"></span></h2>
      <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_recommended_title.png" alt=""></div>
      <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
    </div>

    <div class="case_cards_content stripe-sky">
      <div>
        <div class="temp-none case_cards_narrow">
          <span class="search_icon">絞り込む</span>

          <label class="select-box">
            <select id="area_select">
              <option value="" selected="selected" hidden>
                選択してください
              </option>
              <optgroup label="関東">
                関東
                <option value="東京23区">東京23区</option>
                <option value="東京23区外">東京23区外</option>
                <option value="神奈川県">神奈川県</option>
                <option value="埼玉県">埼玉県</option>
                <option value="千葉県">千葉県</option>
              </optgroup>
            </select>
          </label>

          <label class="select-box">
            <select id="area_select">
              <option value="" selected="selected" hidden>
                選択してください
              </option>
              <optgroup label="関東">
                関東
                <option value="東京23区">東京23区</option>
                <option value="東京23区外">東京23区外</option>
                <option value="神奈川県">神奈川県</option>
                <option value="埼玉県">埼玉県</option>
                <option value="千葉県">千葉県</option>
              </optgroup>
            </select>
          </label>
        </div>

        <div class="case_card_box">
          <ul>
            <li class="case_card">
              <div class="card_icon-img">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/case/icon_face01.png">
              </div>
              <div class="case_card_txt">
                <div class="case_card_status">
                  <div class="age">20代前半</div>
                  <div class="sex">女性</div>
                </div>
                <div class="case_card_detail">
                  <p>
                    メールで診療先の連絡が来る、という事で始めは少し不安でしたが、実際に行ってみたらスタッフさんの対応も良く、店舗も綺麗で安心して施術を受ける事が出来ました！
                  </p>
                </div>
              </div>
            </li>


            <li class="case_card">
              <div class="card_icon-img">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/case/icon_face02.png">
              </div>
              <div class="case_card_txt">
                <div class="case_card_status">
                  <div class="age">30代前半</div>
                  <div class="sex">女性</div>
                </div>
                <div class="case_card_detail">
                  <p>
                    施術完了前に引っ越すことになってしまったのですが、引越し先の近くにも店舗があったので、面倒な乗り換えをせずに済んで助かりました。
                  </p>
                </div>
              </div>
            </li>

            <li class="case_card">
              <div class="card_icon-img">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/case/icon_face03.png">
              </div>
              <div class="case_card_txt">
                <div class="case_card_status">
                  <div class="age">20代後半</div>
                  <div class="sex">女性</div>
                </div>
                <div class="case_card_detail">
                  <p>
                    他のサロンから医療脱毛に乗り換えたい、と思った時にツルツルクリニックを発見して、申し込んでみました。乗り換え割引キャンペーンをやっていたので、お得に脱毛をしてもらう事が出来ていて、とても満足です！店舗が多いので、勤務先から家までの間で通う事が出来ているのも続けられるポイントかな、と思います。
                  </p>
                </div>
              </div>
            </li>

          </ul>
        </div>
      </div>
    </div>

  </section>
</div>

<?php
get_footer();
