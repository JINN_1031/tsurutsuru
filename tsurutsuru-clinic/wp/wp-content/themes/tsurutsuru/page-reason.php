<?php get_header(); ?>

<section class="about_sec1">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>選ばれる理由<span class="title_dec-r"></span></h2>
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_reason_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>
  <div class="reason_jump_wrapper">
    <ul>
      <a href="#reason01">
        <li class="reason_jump_card">
          <div class="reason_jump_card_num">
            <div>01</div>
          </div>
          <p class="reason_jump_card_txt">きれいなお肌をみんなで目指す</p>
          <div class="jump-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_arrow.png"></div>
        </li>
      </a>

      <a href="#reason02">
        <li class="reason_jump_card">
          <div class="reason_jump_card_num">
            <div>02</div>
          </div>
          <p class="reason_jump_card_txt">安心で快適、いつでも心地よく</p>
          <div class="jump-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_arrow.png"></div>
        </li>
      </a>

      <a href="#reason03">
        <li class="reason_jump_card">
          <div class="reason_jump_card_num">
            <div>03</div>
          </div>
          <p class="reason_jump_card_txt">高い専門性と優れた技術</p>
          <div class="jump-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_arrow.png"></div>
        </li>
      </a>

      <a href="#reason04">
        <li class="reason_jump_card">
          <div class="reason_jump_card_num">
            <div>04</div>
          </div>
          <p class="reason_jump_card_txt">「痛い」を可能な限り軽減するための取り組み</p>
          <div class="jump-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_arrow.png"></div>
        </li>
      </a>

      <a href="#reason05">
        <li class="reason_jump_card">
          <div class="reason_jump_card_num">
            <div>05</div>
          </div>
          <p class="reason_jump_card_txt">予約が取りやすいクリニック作り</p>
          <div class="jump-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/dec_arrow.png"></div>
        </li>
      </a>
    </ul>
  </div>
</section>

<div class="bg-sky">
  <section>
    <div class="reason_wrap">
      <ul>

        <li id="reason01" class="stripe-sky">
          <div class="reason-card">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/reason-first.jpg" alt="">
            </div>
            <div>
              <h3>きれいなお肌をみんなで目指す</h3>
              <p>ドクター・カウンセラーなどスタッフが一丸になって、患者さまを美しい肌へと導きます。単なる脱毛でなく、心身ともに女性がきれいになるということにこだわるクリニックです。<br>ノースリーブを着たい、ミニスカートを履きたい、水着をもっと着こなしたいなど、皆さまの要望を聞かせてください。どうなりたいか？という目標を一緒に見据えながら、カウンセラーが最適なプランをご提案させていただきます。</p>
            </div>
          </div>
        </li>
        <li id="reason02" class="stripe-sky">
          <div class="reason-card">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/reason-second.jpg" alt="">
            </div>
            <div>
              <h3>安心で快適、いつでも心地よく</h3>
              <p>追加料金にNO。キャンセル料金にNO。曖昧でわかりにくい料金設定を徹底排除。ツルツルクリニックは明確な料金設定と丁寧なご説明が何よりと考え、患者さまからの質問にはじっくりと時間をかけてお答えしています。<br>カウンセリング時はもちろん、通院を開始してからも疑問や不安が出てきましたら、遠慮なくお尋ねください。料金に関することだけでなく、脱毛や肌に関するお悩みもツルツルっとゼロへ。</p>
            </div>
          </div>
        </li>
        <li id="reason03" class="stripe-sky">
          <div class="reason-card">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/reason-third.jpg" alt="">
            </div>
            <div>
              <h3>高い専門性と優れた技術</h3>
              <p>医療脱毛専門のクリニックとして、スタッフひとりひとりが責任を持って技術を学び磨いています。お体の形状、お肌の質感、毛の太さや量などは患者さまごとに異なりますが、どんな方にでも高い効果を発揮できるよう経験豊富なドクター・カウンセラーが対応させていただきます。<br>痛みと肌へのダメージの軽減にも科学的なアプローチで対応してまいります。専門クリニックならではの質の高さをぜひご体感ください。</p>
            </div>
          </div>
        </li>
        <li id="reason04" class="stripe-sky">
          <div class="reason-card">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/reason-fourth.jpg" alt="">
            </div>
            <div>
              <h3>「痛い」を可能な限り軽減するための取り組み</h3>
              <p>医療レーザー脱毛は少なからず痛みを伴う施術です。「医療レーザー脱毛は痛い。だからこそ、お客様の「痛い」を可能な限り軽減する」というのがツルツルクリニックの施術方針です。「どのように照射すれば患者さまにとって一番楽か」ということを常に意識しながら施術に取り組んでおります。</p>
            </div>
          </div>
        </li>
        <li id="reason05" class="stripe-sky">
          <div class="reason-card">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/reason/reason-fifth.jpg" alt="">
            </div>
            <div>
              <h3>予約が取りやすいクリニック作り</h3>
              <p>予約を取りやすくするために、再診患者さまを最優先に考えたクリニック作りを行っております。
                現在通われている院でのご予約が取りづらい場合には、ご契約された院とは別の院のご案内をさせていただくことも可能です。（一部院を除く）<br>ツルツルクリニックでは患者さまの「不安」を「安心」に変えるために、ご予約時の電話対応１つから患者さま目線での対応を心がけております。</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </section>
</div>

<?php
get_footer();
