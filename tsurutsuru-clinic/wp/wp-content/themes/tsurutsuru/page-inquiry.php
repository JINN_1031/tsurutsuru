<?php
/*
Template Name: お問い合わせ
*/
?>

<?php get_header(); ?>

<section class="inquiry_sec">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>お問い合わせ<span class="title_dec-r"></span></h2>
    <!-- *****リボンがまだ↓ -->
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/inquiry/dec_inquiry_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>

  <div class="form_table">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; endif; ?>
  </div>
</section>

<?php
get_footer();
