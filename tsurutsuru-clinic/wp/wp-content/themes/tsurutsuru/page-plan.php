<?php get_header(); ?>

<div class="outline-sky">
  <section class="about_sec3">
    <div class="popular-plan_wrap">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>プラン一覧<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/dec_popular_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>

      <p>気軽にツルツルが目指せる！最低3回から脇だけ、うなじだけなら15,000円からご案内可能！<br>脱毛範囲と回数の組み合わせが豊富なのがツルツルクリニックの強み。<br>患者さまそれぞれの状態とご希望に合わせて最適なプランを提案します。</p>

      <div class="popular-plan_card_container">
        <ul>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">全身くまなく！まるっとツルツルPLAN</h3>
            <p>文字通り当院で照射できるすべてのパーツを脱毛します。顔やVIOも含まれていますが、もちろんご希望によって照射をしないという選択もできます。<br>また、他のサロンで脱毛を終えている部位が多い場合、割引料金でご案内できることも。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：240,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：400,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：680,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・回数無制限のコースには学割、乗り換え割りのご用意も！</span>
          </li>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">気になるところだけ！お手軽半身PLAN</h3>
            <p>上半身だけ、中半身だけ、下半身だけの３種類から選べます。<br>自分では手の届きにくいところ、セルフお手入れに不安があるところだけ脱毛することが可能です。<br>費用を抑えながら、ツルツルの体を手に入れたい方に。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：120,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：200,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：300,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・回数無制限のコースには学割、乗り換え割りのご用意も！</span>
          </li>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">まずはお試し！ここだけ部分PLAN</h3>
            <p>腕だけ、脚だけ、うなじだけ、脇だけ、VIOだけ…などなど、ご希望に合わせて費用と期間をカスタマイズ。<br>狭い範囲への照射となるため、医療脱毛が初めての方、痛みや肌荒れが心配な方にもおすすめです。<br>部分PLAN終了後「もっとツルツルになりたい！」と感じた患者さまには、割引価格で全身PLANにご案内しております。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：15,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：20,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全8回：30,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・全８回のコースには学割、乗り換え割りのご用意も！</span>
          </li>
        </ul>
      </div>
      <div class="popular-plan_comment">
        <div>
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/about/img_comment.png" alt="">
        </div>
        <div class="popular-plan_comment_txt">
          <p>どのプランを選ぼうか悩んでいる、何回通えばいいのかわからない、そんな方も安心してカウンセリングにお越しください。<br>経験豊富なカウンセラーがおひとりおひとりにぴったりのプランをご紹介します。<br>もちろん、その場での契約を迫るようなことはありませんので、ゆっくりじっくり考えていただけます。</p>
        </div>
      </div>
    </div>
</section>
</div>

<?php
get_footer();