<?php
/**
 * サイドバー
 *
 * @package WordPress
 */
?>

<div class="sidemenu">
  <div class="category-list">
    <h3 class="side_title">ツルツル脱毛ガイド</h3>
    <div class="side_contents">
      <ul>

        <li>
          <dl>
            <input type="checkbox" id="cate01">
            <dt>
              <label for="cate01">医療脱毛、部位別情報まとめ</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/">医療脱毛、部位別情報まとめ</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/face/">「顔」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/beard/">「ヒゲ」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/sideburns/">「もみあげ」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/jaw/">「あご」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/undernose/">「鼻下」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/forehead/">「おでこ」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/eyebrow/">「眉毛」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/vibrissa/">「鼻毛」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/upper-body/">「上半身」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/back/">「背中」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/aside/">「ワキ」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/breast/">「胸」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/nipple/">「乳首・乳輪」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/belly/">「お腹」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/arm/">「腕」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/hand/">「手の甲・指」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/lower-body/">「下半身」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/vio/">「VIO」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/v-line/">「Vライン」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/i-line/">「Iライン」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/o-line/">「Oライン」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/leg/">「足・ひざ下」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/hip/">「お尻」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/thigh/">「太もも」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>parts/body/">「全身」医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/cycle/">部位別の毛周期</a>
            </dd>
          </dl>
        </li>
        <li>
          <dl>
            <input type="checkbox" id="cate02">
            <dt>
              <label for="cate02">医療脱毛お役立ち情報</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>time/">回数と期間</a>
              <a href="<?php echo esc_url(home_url('/')); ?>effect/">費用と効果</a>
              <a href="<?php echo esc_url(home_url('/')); ?>merit/">メリット・デメリット</a>
              <a href="<?php echo esc_url(home_url('/')); ?>money/">相場</a>
              <a href="<?php echo esc_url(home_url('/')); ?>self/">毛の自己処理</a>
            <dd>
          </dl>
        </li>
        <li>
          <dl>
            <input type="checkbox" id="cate03">
            <dt>
              <label for="cate03">年齢別、医療脱毛情報</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>age/">年齢別、医療脱毛情報 TOP</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/child/">小学生以下の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/junior-high-school/">中学生の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/high-school/">高校生の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/20th/">２０代の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/30th/">３０代の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/40th/">４０代の医療脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>age/50th/">５０代の医療脱毛</a>
            </dd>
          </dl>
        </li>
        <li>
          <dl>
            <input type="checkbox" id="cate04">
            <dt>
              <label for="cate04">医療脱毛レーザーについて</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/">医療脱毛レーザー TOP</a>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/kind/">レーザーの種類</a>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/pain/">レーザーの強さと痛み</a>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/cost/">レーザーの費用と効果</a>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/light/">レーザーと光脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>machine/mole/">レーザーとほくろ</a>
          <a href="<?php echo esc_url(home_url('/')); ?>machine/forever/">レーザー脱毛は永久脱毛？</a>
            </dd>
          </dl>
        </li>
        <li>
          <dl>
            <input type="checkbox" id="cate05">
            <dt>
              <label for="cate05">初めての医療脱毛ガイド</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/">初めての医療脱毛ガイド</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/flow/">全体の流れ</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/information/">情報収集</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/standard/">店舗選び</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/counseling/">カウンセリングの流れ</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/operation/">施術の流れ</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/constitution/">アトピー・アレルギー</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/self/">当日の自己処理</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/self/">施術後の自己処理</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/cooling-off/">クーリングオフ</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/experience/">体験脱毛</a>
            </dd>
          </dl>
        </li>
        <li>
          <dl>
            <input type="checkbox" id="cate06">
            <dt>
              <label for="cate06">脱毛Q&amp;A</label></dt>
            <dd>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/start/">医療脱毛、始める時期</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/genetics/">体毛の濃さと遺伝</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/food/">体毛の濃さと食事</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/lifestyle/">体毛の濃さと生活習慣</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/comparison/">部分脱毛・全身脱毛の比較</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/parmanent/">永久脱毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/bury/">埋没毛</a>
              <a href="<?php echo esc_url(home_url('/')); ?>beginner/brazilian/">ブラジリアンワックス</a>
            </dd>
          </dl>
        </li>
      </ul>
    </div>
  </div>
  <div class="ranking-list">
    <h3 class="side_title">人気記事</h3>
    <div class="side_contents">
      <ul>
        <li>
        <a href="/parts/vio/">
            <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00112_300x300.jpg" class="trouble_a_img"></div>
            <div>
              <span class="color-orange">部位別</span>
            <p>VIO脱毛は医療脱毛がおすすめ！ハイジニーナ脱毛で失敗しないためには？</p>
            </div>
          </a>
        </li>
        <li>
        <a href="/parts/aside/">
            <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00071_300x300.jpg" class="trouble_a_img"></div>
            <div>
              <span class="color-orange">部位別</span>
            <p>ワキ脱毛は医療脱毛がおすすめ！安い理由や施術回数を解説</p>
            </div>
          </a>
        </li>
        <li>
        <a href="/beginner/counseling/">
            <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00013_300x300.jpg" class="trouble_a_img"></div>
            <div>
              <span class="color-blue">100%ガイド</span>
            <p>脱毛クリニックのカウンセリングってどんなことを話すの？流れは？</p>
            </div>
          </a>
        </li>
        <li>
          <a href="/merit/">
            <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00004_300x300.jpg" class="trouble_a_img"></div>
            <div>
              <span class="color-pink">メリット</span>
            <p>医療脱毛のメリット・デメリット｜脱毛の気になる情報を一挙に公開</p>
            </div>
          </a>
        </li>
        <li>
        <a href="/beginner/">
            <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00009_300x300.jpg" class="trouble_a_img"></div>
            <div>
              <span class="color-blue">100%ガイド</span>
            <p>【100％ガイド】初めて脱毛クリニックに行く前に知っておきたいこと</p>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
