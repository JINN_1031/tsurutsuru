<?php get_header(); ?>

<section class="qa_sec">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>よくある質問<span class="title_dec-r"></span></h2>
    <!-- *****リボンがまだ↓ -->
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/qa/dec_qa_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>

  <div class="qa_wrapper">
    <div class="qa_block">
      <input id="qa-check1" class="acd-check" type="checkbox" checked>
      <h2><label class="acd-label" for="qa-check1">医療脱毛について<div class="label-status"></div></label></h2>
      <div class="qa_content">
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>光脱毛と医療レーザー脱毛の違いは何ですか？</dt>
          <dd><span class="qa_a">A.</span>医療レーザー脱毛は医師免許を持ったドクターがいる医療機関でのみ受けられます。 また、エステサロンで使用されている光脱毛は、医療レーザーに比べ出力が低く脱毛効果を得るまでに回数が必要です。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>VIO脱毛は、粘膜部分まで照射できますか？</dt>
          <dd><span class="qa_a">A.</span>お肌や毛の状態によって、粘膜ぎりぎりか粘膜部分まで照射をおこなっています。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>照射を何回おこなえば、毛がなくなりますか？</dt>
          <dd><span class="qa_a">A.</span>個人差はありますが、ワキなど毛質の太い部位は約5～6回程度、腕・脚など少々細い毛質の部位は約6～8回程度、顔や体毛の産毛などの部位は8～10回以上の照射が必要になる場合があります。</dd>
        </dl>
      </div>
    </div>

    <div class="qa_block">
      <input id="qa-check2" class="acd-check" type="checkbox">
      <h2><label class="acd-label" for="qa-check2">支払い方法について<div class="label-status"></div></label></h2>
      <div class="qa_content">
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>月額払いとは何ですか？</dt>
          <dd><span class="qa_a">A.</span>医療ローンを利用する方法です。普通のショッピングローンと同様に、審査に通過するとご利用いただけるローンで、毎月ご指定の口座から一定額が引き落としされるので、脱毛に通いながらお支払いすることができます。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>支払いにクレジットカードは使えますか？</dt>
          <dd><span class="qa_a">A.</span>はい、ご利用いただけます。支払い方法：1回払いのみ</dd>
        </dl>
        
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>分割で支払うことはできますか？</dt>
          <dd><span class="qa_a">A.</span>医療ローンをご利用の場合、分割払いが可能です。 分割可能回数は施術によって異なりますので、スタッフまでお問い合わせください。</dd>
        </dl>
        
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>医療ローンを申し込むのに、必要なものはありますか？</dt>
          <dd><span class="qa_a">A.</span>医療ローンをお考えの方は、カウンセリング来院時に以下をご用意ください。
          <br>・本人確認書類(ご本人さまと現住所が確認できるもの：免許証・健康保険証・パスポート・住基カードなど)
          <br>・印鑑(銀行届出印と同じもの)
          <br>・通帳
          <br>・お勤め先の名称・住所・電話番号などのメモ(アルバイト・パート・派遣も可能)</dd>
        </dl>
      </div>
    </div>
    <div class="qa_block">
      <input id="qa-check3" class="acd-check" type="checkbox">
      <h2><label class="acd-label" for="qa-check3">契約について<div class="label-status"></div></label></h2>
      <div class="qa_content">
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>引越などで通えなくなった場合、解約できますか？</dt>
          <dd><span class="qa_a">A.</span>解約できます。お支払い方法により別途手数料がかかる場合もございますので、スタッフまでご確認ください。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>未成年者でも脱毛できますか？</dt>
          <dd><span class="qa_a">A.</span>未成年の方でも16歳以上であれば下記条件にて脱毛できます。
          <br>【親権者さまと同伴にてご来院される場合】
          <br>− 当日ご契約ができ、ご契約金額や支払い方法に制限はございません。
          <br>【親権者さまと同伴にてご来院できない場合】
          <br>− 親権者さまの同意書(親権者さまの自筆での署名・捺印)をお持ちいただき、ご契約となります。</dd>
        </dl>

      </div>
    </div>
    <div class="qa_block">
      <input id="qa-check4" class="acd-check" type="checkbox">
      <h2><label class="acd-label" for="qa-check4">照射について<div class="label-status"></div></label></h2>
      <div class="qa_content">
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>カウンセリング当日の照射は可能ですか？</dt>
          <dd><span class="qa_a">A.</span>ご契約内容によりご案内が可能です。詳しくは各クリニックまたはお問い合わせ窓口までご連絡ください。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>照射当日はどのような格好で行けば良いですか？</dt>
          <dd><span class="qa_a">A.</span>特に指定はございません。</dd>
        </dl>

        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>顔脱毛の場合、メイクをしたままでも大丈夫ですか？</dt>
          <dd><span class="qa_a">A.</span>脱毛箇所のメイクは落としていただきます。照射後にはメイクをしてお帰りいただく事が可能です。</dd>
        </dl>
      </div>
    </div>
    <div class="qa_block">
      <input id="qa-check6" class="acd-check" type="checkbox">
      <h2><label class="acd-label" for="qa-check6">その他<div class="label-status"></div></label></h2>
      <div class="qa_content">
        <dl class="qa_question">
          <dt><span class="qa_q">Q.</span>休診日はありますか？</dt>
          <dd><span class="qa_a">A.</span>各院によって休診日は異なります。詳しくはお問い合わせ窓口までご連絡ください。</dd>
        </dl>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
