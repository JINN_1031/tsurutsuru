<?php
/*
Template Name: 記事テンプレート(固定ページ)
*/
?>
<?php get_header(); ?>

<div class="bg-lowsky">
  <section class="article_wrap">
    <div class="article_contents_box stripe-sky">
      <div class="article_contents">

      <?php
        if ( have_posts() ) :
        while ( have_posts() ) : the_post();
      ?>

        <h1>
          <?php the_title(); ?>
        </h1>

        <div class="sns-box">
          <a href="###"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_snsm_twitter.png"></a>
          <a href="###"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_snsm_line.png"></a>
          <a href="###"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_snsm_fb.png"></a>
          <a href="###"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_snsm_b.png"></a>
        </div>

        <?php the_post_thumbnail(); ?>

        <?php the_content(); ?>

        <?php 
          endwhile;
          endif;
        ?>

        <div class="related_wrap article_links">
          <?php 
            $url1 = get_post_meta($post->ID, 'related1', true);
            $id1 = url_to_postid( $url1 );
            $url2 = get_post_meta($post->ID, 'related2', true);
            $id2 = url_to_postid( $url2 );
            $url3 = get_post_meta($post->ID, 'related3', true);
            $id3 = url_to_postid( $url3 );
            // 記事ID３つ配列にぶち込む
            $page_id = array( $id1,$id2,$id3 );
            // 記事の数
            $page_id_num = 3;
            // 表示
            if ($url1 !== ''){
              echo '<h2>関連記事</h2><ul>';
              for($i = 0; $i < $page_id_num; $i++) {
                echo '<li>';
                echo '<a href="' . get_permalink( $page_id[$i] ) . '">';// 記事URL
                echo '<div class="related_thumbnail">' . get_the_post_thumbnail( $page_id[$i], array(210, 104) ) . '</div>';// サムネイル画像
                echo '<p>' . get_post( $page_id[$i] )->post_title . '</p>';// 記事タイトル
                echo '</a>';
                echo '</li>';
              }
              echo '</ul>';
            }
          ?>
        </div>

        <div class="caetgory-archive_wrap article_links">
          <h2>カテゴリー 一覧</h2>
          <ul>
            <li>
            <a href="/parts/">
                <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00001_300x300.jpg" alt=""></div>
              <div>部位別情報まとめ</div>
              </a>
            </li>
            <li>
            <a href="/age/">
                <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00005_300x300.jpg" alt=""></div>
              <div>年齢別、脱毛の疑問</div>
              </a>
            </li>
            <li>
            <a href="/machine/">
                <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00006_300x300.jpg" alt=""></div>
              <div>脱毛レーザーについて</div>
              </a>
            </li>
            <li>
            <a href="/beginner/">
                <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/article/tt00009_300x300.jpg" alt=""></div>
              <div>初めての医療脱毛</div>
              </a>
            </li>
            <li>
            </li>
            <li>
            </li>
          </ul>
        </div>

      </div>
    </div>

    <?php get_sidebar(); ?>

  </section>
</div>

<?php
get_footer();
