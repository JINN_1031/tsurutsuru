<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after.
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
</main>

<footer>
  <div class="footer_wrap">
    <nav class="footer-nav_wrap">
      <div class="footer-logo">
        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/logo_w.png" alt=""></a>
      </div>
      <div class="footer-links">
        <div class="footer-row">
          <ul>
            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
            <li><a href="/plan">脱毛プラン一覧</a></li>
            <li><a href="/beginner">初めての方へ</a></li>
            <li><a href="/column">脱毛コラム</a></li>
            <li><a href="/clinic" class="temp-none">クリニック検索</a></li>
          </ul>
        </div>
        <div class="footer-row">
          <ul>
            <li><a href="/counseling" target="_blank">無料カウンセリング</a></li>
            <li><a href="/qa">よくある質問</a></li>
            <li><a href="/inquiry">お問い合わせ</a></li>
            <li></li>
            <li></li>
          </ul>
        </div>
        <div class="footer-row">
          <ul>
            <li><a href="/privacy-policy">プライバシーポリシー</a></li>
            <li><a href="/terms">利用規約</a></li>
            <li><a href="/recruit" class="temp-none">採用情報</a></li>
            <li><a href="/company" class="temp-none">会社概要</a></li>
            <li><a href="/sitemap" class="temp-none">サイトマップ</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <div class="footer-copyright">
    <span class="copyright">Copyright © ツルツルクリニック All Rights Reserved.</span>
    <div class="footer-sns">
      <ul>
        <li>
          <a href="twitter"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_sns_twitter.png" alt="facebook"></a>
        </li>
        <li>
          <a href="facebook"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_sns_fb.png" alt="facebook"></a>
        </li>
        <li>
          <a href="insta"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_sns_insta.png" alt="facebook"></a>
        </li>
      </ul>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
<script src="<?php echo resolve_asset_url('/js/app.js'); ?>" async></script>
</body>

</html>
