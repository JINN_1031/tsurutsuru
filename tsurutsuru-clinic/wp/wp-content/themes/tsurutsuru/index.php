<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @see https://codex.wordpress.org/Template_Hierarchy
 */
get_header(); ?>

<section>
  <div class="about_box about1">
    <div class="about_img">
      <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top_img01.jpg" alt="">
    </div>
    <div class="about_text">
      <h2><span class="about_h2-icon"></span>クリニックについて</h2>
      <p>脱毛して叶えたい「理想のあなた」を教えてください。<br>ツルツルクリニック は、ただ毛を処理する場所ではなく、ドクター・カウンセラーとともに美しい肌を目指す美容クリニックです。<br>医療脱毛が初めての方も他の脱毛サロンで苦い思い出がある方もご安心を。初回カウンセリング時から通院時まで、どんな段階のどんな心配ごとも、私たちが素早くツルツルっと解決いたします。</p>
      <a href="/about" class="bg-orange button_wrap">
        <span class="button_inner">
          <span>
            当院について
          </span>
        </span>
      </a>
    </div>
  </div>

  <div class="about_box about2">
    <div class="about_img">
      <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top_img02.jpg" alt="">
    </div>
    <div class="about_text">
      <h2><span class="about_h2-icon"></span>全国展開</h2>
      <p>1回の通院では終わらないことが多い脱毛。<br>クリニック選びには通いやすさが重要です。<br>首都圏を中心にツルツルクリニックはどんどん拡大中。<br>ご自宅やお勤め先から通いやすい院をお選びいただけます。</p>
      <a href="/counseling" class="bg-pink button_wrap">
        <span class="button_inner">
          <span>
            無料カウンセリングを予約
          </span>
        </span>
      </a>
    </div>
  </div>

  <div class="about_box about3">
    <div class="about_img">
      <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top_img03.jpg" alt="">
    </div>
    <div class="about_text">
      <h2><span class="about_h2-icon"></span>選ばれる理由</h2>
      <p>選ばれるのにはわけがある。ツルツルクリニックは高い専門性を持った医療脱毛専門クリニックであり、皆さまの理想を一緒に追い求めていくパートナーでもあります。<br>派手な宣伝広告や一見安く思える価格設定などで患者さまを惑わすことなく、当院にできることをしっかりとお伝えいたします。<br></p>
      <a href="/reason" class="bg-blue button_wrap">
        <span class="button_inner">
          <span>
            詳細をみる
          </span>
        </span>
      </a>
    </div>
  </div>
</section>

<section>
  <div class="top-trouble">
    <h2>脱毛について、こんなお悩みありませんか？</h2>
    <div class="top-trouble_wrap">
      <ul>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>今まで自宅でカミソリを使っていたけど、肌ダメージが心配で…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">脱毛クリニックなら照射回数もアフターケアもしっかり考えてくれる！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-001.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>以前脱毛サロンに通っていたけど、あまり効果を実感できなかった…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">医療脱毛専門のクリニックだから、美容サロンとは質が違う！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-002.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>脱毛体験をしたことがあるけど、痛すぎて続けられなかった…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">医療脱毛は痛みが最小限！不安があってもしっかり相談に乗ってもらえます！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-003.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>回数とか脱毛部位とかよくわからなくて、自分だけでは決められない…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">カウンセリングが丁寧なので大丈夫！悩みをじっくり聞いてくれる！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-004.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>追加料金とかキャンセル料金とかが高そうで怖い…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">お金の不安もわかりやすく解決してくれるから安心できる！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-005.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>脱毛の後のケアって難しそう…美肌を目指したい…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">アフターケアも充実で、お肌に関するいろんなことが聞けます！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-006.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>体毛が濃かったり薄かったりどんな人でも医療脱毛ってできるのかなあ…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">経験豊富なカウンセラーとドクターがいるから安心！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-007.jpg"></div>
            </div>
          </div>
        </li>
        <li class="stripe-sky">
          <div class="top-trouble_card">
            <p class="trouble_q">
              <span>他のサロンやクリニックとゆっくり比較したいのだけど…</span>
            </p>
            <div class="trouble_a">
              <p class="trouble_a_txt">無理な勧誘は一切なし！わかりやすく丁寧で快適なカウンセリング！</p>
              <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-voice-008.jpg"></div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>

<div class="top-recommended outline-sky">
  <section>
    <div>
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>おすすめ医療脱毛<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_recommended_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>
      <div class="top-recommended_card_wrap">
        <div class="top-recommended_card_list">
          <ul>
            <li class="top-recommended_card">
              <a href="###" class="temp_card-disable">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-plan-full-body.jpg" class="top-recommended_card_img" alt="">
                <h3>全身脱毛</h3>
                <p>ツルツルクリニックの「全身脱毛」は、患者さまのあらゆるニーズに応えるためカウンセリングを元にぴったりなプランを組むことができます。</p>
                <p class="card-price">¥50,000〜 / 回</p>
              </a>
            </li>
            <li class="top-recommended_card">
              <a href="###" class="temp_card-disable">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-plan-aside.jpg" class="top-recommended_card_img" alt="">
                <h3>両脇脱毛</h3>
                <p>脱毛部位として人気のワキ脱毛。手軽に取り組める部位だからこそ、クリニックでの医療脱毛がおすすめです。</p>
                <p class="card-price">¥5,000〜 / 回</p>
              </a>
            </li>
            <li class="top-recommended_card">
              <a href="###" class="temp_card-disable">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-plan-vio.jpg" class="top-recommended_card_img" alt="">
                <h3>VIO脱毛</h3>
                <p>デリケートな部位だからこそ、効果・通う回数・肌トラブル発生時の対応を総合的に考えて、クリニックでの医療脱毛をおすすめします。</p>
                <p class="card-price">¥20,000〜 / 回</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</div>


<section>
  <div class="top_news_wrap">
    <div class="information col-2">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>お知らせ<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_info_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>
      <div class="archive_box">
        <?php
            $arg = array(
              'posts_per_page' => 4, // 表示する件数
              'orderby' => 'date', // 日付でソート
              'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
              'category_name' => 'category-news' // カテゴリーのスラッグを指定
            );
            $posts = get_posts( $arg );
          if( $posts ): ?>
        <ul>
          <?php
                foreach ( $posts as $post ) :
              setup_postdata( $post ); ?>
          <li>
            <div class="archive_info">
              <div class="date">
                <?php the_time( 'Y.m.d' ); ?>
              </div>
              <div class="tag bg-pink">
                <?php // 一つ目のタグ名だけ表示
                $posttags = get_the_tags();
                if ( $posttags[0] ) {
                  echo $posttags[0]->name;
                }
                ?>
              </div>
            </div>
            <div class="archive_title">
              <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
              </a>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php
            endif;
            wp_reset_postdata();
          ?>
        <div class="archive_link">
          <a href="###">一覧へ</a>
        </div>
      </div>
    </div>

    <div class="campaign col-2">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>キャンペーン<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_camp_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>
      <div class="archive_box">
        <?php
            $arg = array(
              'posts_per_page' => 4, // 表示する件数
              'orderby' => 'date', // 日付でソート
              'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
              'category_name' => 'category-campaign' // カテゴリーのスラッグを指定
            );
            $posts = get_posts( $arg );
          if( $posts ): ?>
        <ul>
          <?php
                foreach ( $posts as $post ) :
              setup_postdata( $post ); ?>
          <li>
            <div class="archive_info">
              <div class="date">
                <?php the_time( 'Y.m.d' ); ?>
              </div>
              <div class="tag bg-orange">
                <?php // 一つ目のタグ名だけ表示
                  $posttags = get_the_tags();
                  if ( $posttags[0] ) {
                    echo $posttags[0]->name;
                  }
                ?>
              </div>
            </div>
            <div class="archive_title">
              <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
              </a>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php
            endif;
            wp_reset_postdata();
          ?>
        <div class="archive_link">
          <a href="###">一覧へ</a>
        </div>

      </div>
    </div>
  </div>
</section>

<div class="top-recommended outline-sky">
  <section>
    <div>
      <div class="top_column_wrap">
        <div class="title-wrap">
          <h2><span class="title_dec-l"></span>脱毛コラム<span class="title_dec-r"></span></h2>
          <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_recommended_title.png" alt=""></div>
          <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
        </div>
        <div class="top_column_card_wrap">
          <div class="top_column_card_list">
            <ul>
              <li class="top_column_card">
              <a href="/parts/">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-article-001.jpg" class="top-column_card_img" alt="">
                  <div class="top_column_card_txt">
                    <div class="top-column_card_tag bg-orange">部位別</div>
                    <p class="date">2019.09.01</p>
                  <p>医療脱毛とエステ脱毛、部位ごとの脱毛ならどっち？その疑問、お答えします！</p>
                  </div>
                </a>
              </li>
              <li class="top_column_card">
              <a href="/merit/">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-article-002.jpg" class="top-column_card_img" alt="">
                  <div class="top_column_card_txt">
                    <div class="top-column_card_tag bg-pink">メリット</div>
                  <p class="date">2019.09.01</p>
                  <p>医療脱毛のメリット・デメリット｜脱毛の気になる情報を一挙に公開</p>
                  </div>
                </a>
              </li>
              <li class="top_column_card">
              <a href="/beginner/">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top-article-003.jpg" class="top-column_card_img" alt="">
                  <div class="top_column_card_txt">
                    <div class="top-column_card_tag bg-blue">100%ガイド</div>
                  <p class="date">2019.09.01</p>
                  <p>【100％ガイド】初めて脱毛クリニックに行く前に知っておきたいこと</p>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<section class="temp-none top_info_sec">
  <div class="top_info_wrap">
    <div class="performer_wrap">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>有名人来店情報<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_info_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>
      <div class="archive_box">
        ほげ
      </div>
    </div>

    <div class="info_wrap">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>クリニック情報<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_camp_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>
      <div class="archive_box">
        <div class="area_info_inner stripe-sky">
          <div>
            <div class="info_area">東北</div>
            <ul>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
            </ul>
          </div>
        </div>

        <div class="area_info_inner stripe-sky">
          <div>
            <div class="info_area">東北</div>
            <ul>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
              <li><a href="###">青森院</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="top-footer-img">
  <div>
    <img src="<?php bloginfo('template_directory'); ?>/assets/images/top/top_footer.jpg" class="">
  </div>
</div>

<?php
get_footer();
