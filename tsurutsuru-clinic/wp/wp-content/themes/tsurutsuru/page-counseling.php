<?php
/*
Template Name: 無料カウンセリング
*/
?>
<?php get_header(); ?>

<section class="counseling_calender">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>無料カウンセリング<span class="title_dec-r"></span></h2>
    <!-- *****リボンがまだ↓ -->
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/counseling/dec_counseling_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>
  <h2>第3希望日まで選択してください</h2>
  <p class="alart_txt">※受診いただくクリニックについては、お申し込み後にご案内させていただきます。</p>
  <div class="cal-view_container"></div>
  <div class="form_table">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>
    <?php endwhile; endif; ?>

    <div class="terms-link"><a href="<?php echo esc_url(home_url('/')); ?>terms">利用規約はこちら</a></div>

  </div>
</section>

<?php
get_footer();