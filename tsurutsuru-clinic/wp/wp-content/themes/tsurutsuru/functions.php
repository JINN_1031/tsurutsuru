<?php
/**
 * Theme version
 * style.cssで定義されているバージョン番号を定数化しておきます。
 */
define('THEME_VERSION', wp_get_theme()->get('Version'));

/*
 * functionsフォルダにあるファイルをすべて読み込む
*/
foreach (glob(TEMPLATEPATH.'/private/functions/*.php') as $file) {
    require_once $file;
}

// アイキャッチ画像追加
add_theme_support('post-thumbnails');


//phpをショートコードで読み込む
function short_php($params = array()) {
  extract(shortcode_atts(array(
    'file' => 'default'
  ), $params));
  ob_start();
  include(get_theme_root() . '/' . get_template() . "/$file.php");
  return ob_get_clean();
}
add_shortcode('myphp1', 'short_php');


/*
// 投稿タイプ追加
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'news', [ // 投稿タイプ名の定義
        'labels' => [
            'name'          => 'キャンペーン,　　お知らせ',
            'singular_name' => 'news',    // カスタム投稿の識別名
        ],
        'public'        => true,  
        'has_archive'   => false,
        'menu_position' => 5,
        'show_in_rest'  => true, 
    ]);
}
*/

// 固定カスタムフィールドボックス
function add_related_fields() {
  add_meta_box( 'related_setting', '関連記事', 'insert_related_fields', 'page', 'normal');
}
add_action('admin_menu', 'add_related_fields');

// カスタムフィールドの入力エリア
function insert_related_fields() {
	global $post;
	echo '関連記事1(URL)：<input type="text" name="related1" value="'.get_post_meta($post->ID, 'related1', true).'" size="50" /><br>';
	echo '関連記事2(URL)：<input type="text" name="related2" value="'.get_post_meta($post->ID, 'related2', true).'" size="50" /><br>';
	echo '関連記事3(URL)：<input type="text" name="related3" value="'.get_post_meta($post->ID, 'related3', true).'" size="50" />　<br>';
}
// カスタムフィールドの値を保存
function save_related_fields( $post_id ) {
	if (!empty($_POST['related1'])){
		update_post_meta($post_id, 'related1', $_POST['related1'] );
	} else { 
		delete_post_meta($post_id, 'related1');
	}
	if (!empty($_POST['related2'])){
		update_post_meta($post_id, 'related2', $_POST['related2'] );
	} else {
		delete_post_meta($post_id, 'related2');
	}
	if (!empty($_POST['related3'])){
		update_post_meta($post_id, 'related3', $_POST['related3'] );
	} else {
		delete_post_meta($post_id, 'related3');
	}
}
add_action('save_post', 'save_related_fields');


//ウィジェット対応
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name' => 'サイドバー',
		'id' => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );


/* ↓パンくず生成ソース */
if ( ! function_exists( 'custom_breadcrumb' ) ) {
    function custom_breadcrumb() {
  
      // トップページでは何も出力しないように
      if ( is_front_page() ) return false;
  
      //そのページのWPオブジェクトを取得
      $wp_obj = get_queried_object();
  
      echo '<div id="breadcrumb" class="control-width">'.  //id名などは任意で
        '<ul>'.
          '<li>'.
            '<a href="'. esc_url( home_url() ) .'"><span>ホーム</span></a>'.
          '</li>';
  
      if ( is_attachment() ) {
  
        /**
         * 添付ファイルページ ( $wp_obj : WP_Post )
         * ※ 添付ファイルページでは is_single() も true になるので先に分岐
         */
        $post_title = apply_filters( 'the_title', $wp_obj->post_title );
        echo '<li> > <span>'. esc_html( $post_title ) .'</span></li>';
  
      } elseif ( is_single() ) {
  
        /**
         * 投稿ページ ( $wp_obj : WP_Post )
         */
        $post_id    = $wp_obj->ID;
        $post_type  = $wp_obj->post_type;
        $post_title = apply_filters( 'the_title', $wp_obj->post_title );
  

        /*
        // カスタム投稿タイプかどうか
        if ( $post_type !== 'post' ) {
  
          $the_tax = "";  //そのサイトに合わせて投稿タイプごとに分岐させて明示的に指定してもよい
  
          // 投稿タイプに紐づいたタクソノミーを取得 (投稿フォーマットは除く)
          $tax_array = get_object_taxonomies( $post_type, 'names');
          foreach ($tax_array as $tax_name) {
              if ( $tax_name !== 'post_format' ) {
                  $the_tax = $tax_name;
                  break;
              }
          }
  
          $post_type_link = esc_url( get_post_type_archive_link( $post_type ) );
          $post_type_label = esc_html( get_post_type_object( $post_type )->label );
  
          //カスタム投稿タイプ名の表示
          echo '<li> > '.
                '<a href="'. $post_type_link .'">'.
                  '<span>'. $post_type_label .'</span>'.
                '</a>'.
              '</li>';
          } else {
            $the_tax = 'category';  //通常の投稿の場合、カテゴリーを表示
          }
  
          // 投稿に紐づくタームを全て取得
          $terms = get_the_terms( $post_id, $the_tax );
  
          // タクソノミーが紐づいていれば表示
          if ( $terms !== false ) {
  
            $child_terms  = array();  // 子を持たないタームだけを集める配列
            $parents_list = array();  // 子を持つタームだけを集める配列
  
            //全タームの親IDを取得
            foreach ( $terms as $term ) {
              if ( $term->parent !== 0 ) {
                $parents_list[] = $term->parent;
              }
            }
  
            //親リストに含まれないタームのみ取得
            foreach ( $terms as $term ) {
              if ( ! in_array( $term->term_id, $parents_list ) ) {
                $child_terms[] = $term;
              }
            }
  
            // 最下層のターム配列から一つだけ取得
            $term = $child_terms[0];
  
            if ( $term->parent !== 0 ) {
  
              // 親タームのIDリストを取得
              $parent_array = array_reverse( get_ancestors( $term->term_id, $the_tax ) );
  
              foreach ( $parent_array as $parent_id ) {
                $parent_term = get_term( $parent_id, $the_tax );
                $parent_link = esc_url( get_term_link( $parent_id, $the_tax ) );
                $parent_name = esc_html( $parent_term->name );
                echo '<li> > '.
                      '<a href="'. $parent_link .'">'.
                        '<span>'. $parent_name .'</span>'.
                      '</a>'.
                    '</li>';
              }
            }
  
            $term_link = esc_url( get_term_link( $term->term_id, $the_tax ) );
            $term_name = esc_html( $term->name );
            // 最下層のタームを表示
            echo '<li> > '.
                  '<a href="'. $term_link .'">'.
                    '<span>'. $term_name .'</span>'.
                  '</a>'.
                '</li>';
          }
          */

  
          // 投稿自身の表示
          echo '<li> > <span>'. esc_html( strip_tags( $post_title ) ) .'</span></li>';
  
      } elseif ( is_page() || is_home() ) {
  
        /**
         * 固定ページ ( $wp_obj : WP_Post )
         */
        $page_id    = $wp_obj->ID;
        $page_title = apply_filters( 'the_title', $wp_obj->post_title );
  
        // 親ページがあれば順番に表示
        if ( $wp_obj->post_parent !== 0 ) {
          $parent_array = array_reverse( get_post_ancestors( $page_id ) );
          foreach( $parent_array as $parent_id ) {
            $parent_link = esc_url( get_permalink( $parent_id ) );
            $parent_name = esc_html( get_the_title( $parent_id ) );
            echo '<li> > '.
                  '<a href="'. $parent_link .'">'.
                    '<span>'. $parent_name .'</span>'.
                  '</a>'.
                '</li>';
          }
        }
        // 投稿自身の表示 ここ！！
        echo '<li> > <span>'. esc_html( strip_tags( $page_title ) ) .'</span></li>';
  
      } elseif ( is_post_type_archive() ) {
  
        /**
         * 投稿タイプアーカイブページ ( $wp_obj : WP_Post_Type )
         */
        echo '<li> > <span>'. esc_html( $wp_obj->label ) .'</span></li>';
  
      } elseif ( is_date() ) {
  
        /**
         * 日付アーカイブ ( $wp_obj : null )
         */
        $year  = get_query_var('year');
        $month = get_query_var('monthnum');
        $day   = get_query_var('day');
  
        if ( $day !== 0 ) {
          //日別アーカイブ
          echo '<li> > '.
                '<a href="'. esc_url( get_year_link( $year ) ) .'"><span>'. esc_html( $year ) .'年</span></a>'.
              '</li>'.
              '<li>'.
                '<a href="'. esc_url( get_month_link( $year, $month ) ) . '"><span>'. esc_html( $month ) .'月</span></a>'.
              '</li>'.
              '<li>'.
                '<span>'. esc_html( $day ) .'日</span>'.
              '</li>';
  
        } elseif ( $month !== 0 ) {
          //月別アーカイブ
          echo '<li> > '.
                '<a href="'. esc_url( get_year_link( $year ) ) .'"><span>'. esc_html( $year ) .'年</span></a>'.
              '</li>'.
              '<li>'.
                '<span>'. esc_html( $month ) .'月</span>'.
              '</li>';
  
        } else {
          //年別アーカイブ
          echo '<li> > <span>'. esc_html( $year ) .'年</span></li>';
  
        }
  
      } elseif ( is_author() ) {
  
        /**
         * 投稿者アーカイブ ( $wp_obj : WP_User )
         */
        echo '<li> > <span>'. esc_html( $wp_obj->display_name ) .' の執筆記事</span></li>';
  
      } elseif ( is_archive() ) {
  
        /**
         * タームアーカイブ ( $wp_obj : WP_Term )
         */
        $term_id   = $wp_obj->term_id;
        $term_name = $wp_obj->name;
        $tax_name  = $wp_obj->taxonomy;
  
        /* ここでタクソノミーに紐づくカスタム投稿タイプを出力しても良いでしょう。 */
  
        // 親ページがあれば順番に表示
        if ( $wp_obj->parent !== 0 ) {
  
          $parent_array = array_reverse( get_ancestors( $term_id, $tax_name ) );
          foreach( $parent_array as $parent_id ) {
            $parent_term = get_term( $parent_id, $tax_name );
            $parent_link = esc_url( get_term_link( $parent_id, $tax_name ) );
            $parent_name = esc_html( $parent_term->name );
            echo '<li>'.
                  '<a href="'. $parent_link .'">'.
                    '<span>'. $parent_name .'</span>'.
                  '</a>'.
                '</li>';
          }
        }
  
        // ターム自身の表示
        echo '<li> > '.
              '<span>'. esc_html( $term_name ) .'</span>'.
            '</li>';
  
  
      } elseif ( is_search() ) {
  
        /**
         * 検索結果ページ
         */
        echo '<li> > <span>「'. esc_html( get_search_query() ) .'」で検索した結果</span></li>';
  
      
      } elseif ( is_404() ) {
  
        /**
         * 404ページ
         */
        echo '<li> > <span>お探しの記事は見つかりませんでした。</span></li>';
  
      } else {
  
        /**
         * その他のページ（無いと思うけど一応）
         */
        echo '<li> > <span>'. esc_html( get_the_title() ) .'</span></li>';
  
      }
  
      echo '</ul></div>';  // 冒頭に合わせた閉じタグ
  
    }
  }