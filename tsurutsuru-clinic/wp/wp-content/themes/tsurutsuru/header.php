<?php
/**
 * The header for our theme.
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php bloginfo('name'); wp_title('|', true, 'left'); ?></title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta property="og:locale" content="ja_JP">
  <meta property="fb:admins" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:type" content="blog">
  <meta property="og:image" content="<?php echo resolve_asset_url('/images/common/logo_ogp.png'); ?>">

  <link rel="shortcut icon" href="<?php echo resolve_asset_url('/images/favicon.ico'); ?>">
  <link rel="apple-touch-icon-precomposed" href="<?php echo resolve_asset_url('/images/apple-touch-icon-precomposed.png'); ?>">
  <link rel="stylesheet" href="<?php echo resolve_asset_url('/css/style.css'); ?>">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <header>
    <div class="header-1">
      <input type="checkbox" id="sp-menu_check">

      <h1 class="logo">
        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/header/logo.png"></a>
      </h1>
      <div class="header-top_right">
        <ul>
          <li class="header-top_right_link">
            <a href="/clinic">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
                <image x="0px" y="0px" width="20px" height="20px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA81BMVEWFz/7///+Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/7///8hucG6AAAAT3RSTlMAACWDxuns05pDfq0MpN0PfOB1OTRfvMwbugZ5YIjlqtvPbB8g8C3cSvMn0U9RC/gvosXvAnuEtTMdegjf1/3mrBASwuRmy4WrIUbZFA3o8SfgWAAAAAFiS0dEAf8CLd4AAAAHdElNRQfjCBYEDCSOuYMPAAAA2klEQVQY013Q51bCQBAF4KFIUwldLEDoIIJgp4Qm0kTv+78NM9ndnAP3z06+k7OZGyKOPxC8CIUjUZl9HDlj0Lm88vAaXuKWxgQ/JFPpTDbHw43CPI+3d3IL3fP44GIBKJJOCbBdLAMVg1V+tSZYBxoGqQm0BNvAo4cd4EkwDnSN9Z6BvuAAeDHI272+Cab58ndlMn6o5T95HHwRDUfSQjcaT9yCzlSO2Xyhui+/TfUV72db6i/Rz1posult+WhaPv3dXXQvFfK/rPaBTvMn+k/nagPOOZLllMdHPdcupimRF80AAAAASUVORK5CYII=" />
              </svg>
              クリニック検索
            </a>
          </li>
          <li class="header-top_right_link">
            <a href="/inquiry">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="15px">
                <image x="0px" y="0px" width="20px" height="15px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAPCAMAAADTRh9nAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAwFBMVEWFz/7///+Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/6Fz/7////eRltjAAAAPnRSTlMAAE3e4VT49PP98un7+Q0I/Or3RD3x6+76c2zv/txhpQGeY9Up5tYkHs/sMef2tFJMuxKItbKPUeVXNDXkXD8fUssAAAABYktHRAH/Ai3eAAAAB3RJTUUH4wgWBBEoeGOjOAAAAJtJREFUGNNd0FcOglAUBNB5SlMRsQAW7B1777P/ZQkJKI/5PJnkFuTyzERRoVGXY7AAFiFFiBJhli0ZK7qNKmv1NDYcuvCaZOtvbdLswO/2yP4gtiE5Gvugg8mUs3lEiyWDlVgT3ADWlv4O2Gs8HIXwQtSjzok8X8hrtFKQIG4KeX8gQSOe8XzFy7u/ZuqisGkzyISEqmS/9P58AaQ+HP6zKN9qAAAAAElFTkSuQmCC" />
              </svg>
              お問い合わせ
            </a>
          </li>
          <a href="/counseling" target="_blank" class="counseling_button">
            <div>無料カウンセリング</div>
          </a>
        </ul>

        <?php /* ハンバーガー */ ?>
        <label for="sp-menu_check" class="sp-menu_button">
          <div></div>
          <div></div>
          <div></div>
        </label>
      </div>

      <div class="sp-menu_wrap">
        <div>
          <a href="/about">クリニックについて</a>
          <a href="/reason">選ばれる理由</a>
          <a href="/plan">主要プラン</a>
          <a href="/beginner">初めての方へ</a>
          <a href="/parts">脱毛コラム</a>
          <a href="/case">体験事例</a>
          <a href="/clinic" class="temp-none">クリニック検索</a>
          <a href="/qa">よくある質問</a>
          <a href="/inquiry">お問い合わせ</a>
          <a href="/counseling" class="sp-menu_counseling">
            <div>無料カウンセリング</div>
          </a>
        </div>
      </div>
    </div>

    <div class="header-2">
      <nav class="g-nav">
        <ul>
          <li>
            <a href="/about" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu01.png">
                </div>
                クリニックについて
              </div>
            </a>
          </li>
          <li>
            <a href="/reason" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu02.png">
                </div>
                選ばれる理由
              </div>
            </a>
          </li>
          <li>
            <a href="/plan" class="gnav_link">
              <div>
                <div class="gnav_icon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu03.png"></div>
                主要プラン
              </div>
            </a>
          </li>
          <li>
            <a href="/beginner" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu04.png">
                </div>
                初めての方へ
              </div>
            </a>
          </li>
          <li>
            <a href="/parts" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu05.png">
                </div>
                脱毛コラム
              </div>
            </a>
          </li>
          <li>
            <a href="/case" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu06.png">
                </div>
                体験事例
              </div>
            </a>
          </li>
          <li>
            <a href="/qa" class="gnav_link">
              <div>
                <div class="gnav_icon">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/header/icon_menu07.png">
                </div>
                よくある質問
              </div>
            </a>
          </li>
        </ul>
      </nav>
      <?php if ( is_home() || is_front_page() ) : ?>
      <div class="mv">
        <div class="mv_slider">
          <div><img src="<?php bloginfo('template_directory'); ?>/assets/images/header/header_fv.jpg" alt=""></div>
        </div>
        <div class="mv_txt">ツルツル女子、急増中！<span class="color_yellow">今イチバン人気</span>の脱毛クリニック</div>
      </div>
      <?php endif; ?>
    </div>

    <?php if ( is_home() || is_front_page() ) : ?>
    <div class="member-list_wrap"></div>
    <?php endif; ?>

  </header>

  <main>
    <?php custom_breadcrumb(); ?>
