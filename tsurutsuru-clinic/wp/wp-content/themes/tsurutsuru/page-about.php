<?php get_header(); ?>

<div class="bg-lowsky">
  <section class="about_sec1">
    <h2>もっと手軽に、きれいな肌へ</h2>
    <div class="main-img stripe-sky">
      <img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about.jpg" alt="">
    </div>
    <p>
      ただ毛を抜くというだけでなく、見た目も触り心地もツルツルな肌へ。<br>そしてそのための通院も、嫌々通う面倒な時間ではなく、楽しく心地よいお手入れ時間へ。<br><span class="bg-yellow">脱毛をして叶えたい「理想のあなた」を教えてください。</span><br>ツルツルクリニックは美しくなりたいすべての人に寄り添います。<br>医療脱毛が初めてでも、他のサロンで苦い思い出があっても大丈夫。<br>技術の効果や安全性はもちろん、患者さまとのコミュニケーションにも自信があります。<br>
    </p>
  </section>
</div>

<section class="about_sec2">
  <h2 class="bg-blue">患者さまの心配ごとを<span class="color-yellow">ツルツルっとゼロに</span></h2>
  <div class="about_sec2_wrap">
    <div class="about_sec2_inner">
      <div>
        <img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about-first-child.jpg" alt="">
      </div>
      <div>
        <h3 class="color-blue">初回カウンセリング時のこだわり</h3>
        <p>わからないこと、不安なこと、全部吐き出してください。<br>経験豊富なカウンセラーが丁寧にお答えしていきます。<br>また、無理に高いプランをすすめたり、当日中の契約を迫ったりは決してしません。<br>他のサロンやクリニックと比較検討中、お家に帰ってじっくり考えたいなど、ご要望を遠慮なくお伝えください。</p>
      </div>
    </div>
    <div class="about_sec2_inner">
      <div>
        <img src="<?php bloginfo('template_directory'); ?>/assets/images/about/about-second-child.jpg" alt="">
      </div>
      <div>
        <h3 class="color-blue">初回～数回目の照射時のこだわり</h3>
        <p>脱毛に限らず、初めての治療や施術に不安はつきもの。初回照射前には改めて医療脱毛のメリットや注意点をしっかりご説明します。<br><br>おひとりおひとりに合った照射量で痛みを最小限に。<br>乾燥や赤みが出た方には保湿剤などで最大限のケアを。「怖い」「痛い」「つらい」をなくし、快適に脱毛を進めていただけるよう患者さまに寄り添います。<br><br>もちろん、通院に慣れてきた方や脱毛回数・範囲の追加を考えている方にも、しっかりと時間を取って、誠実に丁寧にご対応させていただきます。どんな段階のどんな心配ごとも、素早くツルツルっと解決するのが私たちのこだわりです。</p>
      </div>
    </div>
  </div>
</section>

<div class="outline-sky">
  <section class="about_sec3">
    <div class="popular-plan_wrap">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>人気プラン<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/dec_popular_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>

      <p>気軽にツルツルが目指せる！最低3回から脇だけ、うなじだけなら15,000円からご案内可能！<br>脱毛範囲と回数の組み合わせが豊富なのがツルツルクリニックの強み。<br>患者さまそれぞれの状態とご希望に合わせて最適なプランを提案します。</p>

      <div class="popular-plan_card_container">
        <ul>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">全身くまなく！まるっとツルツルPLAN</h3>
            <p>文字通り当院で照射できるすべてのパーツを脱毛します。顔やVIOも含まれていますが、もちろんご希望によって照射をしないという選択もできます。<br>また、他のサロンで脱毛を終えている部位が多い場合、割引料金でご案内できることも。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：240,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：400,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：680,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・回数無制限のコースには学割、乗り換え割りのご用意も！</span>
          </li>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">気になるところだけ！お手軽半身PLAN</h3>
            <p>上半身だけ、中半身だけ、下半身だけの３種類から選べます。<br>自分では手の届きにくいところ、セルフお手入れに不安があるところだけ脱毛することが可能です。<br>費用を抑えながら、ツルツルの体を手に入れたい方に。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：120,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：200,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：300,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・回数無制限のコースには学割、乗り換え割りのご用意も！</span>
          </li>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">まずはお試し！ここだけ部分PLAN</h3>
            <p>腕だけ、脚だけ、うなじだけ、脇だけ、VIOだけ…などなど、ご希望に合わせて費用と期間をカスタマイズ。<br>狭い範囲への照射となるため、医療脱毛が初めての方、痛みや肌荒れが心配な方にもおすすめです。<br>部分PLAN終了後「もっとツルツルになりたい！」と感じた患者さまには、割引価格で全身PLANにご案内しております。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：15,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：20,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全8回：30,000円～</div>
              </div>
            </div>
            <span class="price_attention">※全５回・全８回のコースには学割、乗り換え割りのご用意も！</span>
          </li>
        </ul>
      </div>
      <div class="popular-plan_comment">
        <div>
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/about/img_comment.png" alt="">
        </div>
        <div class="popular-plan_comment_txt">
          <p>どのプランを選ぼうか悩んでいる、何回通えばいいのかわからない、そんな方も安心してカウンセリングにお越しください。<br>経験豊富なカウンセラーがおひとりおひとりにぴったりのプランをご紹介します。<br>もちろん、その場での契約を迫るようなことはありませんので、ゆっくりじっくり考えていただけます。</p>
        </div>
      </div>
    </div>
  </section>
</div>

<?php
get_footer();
