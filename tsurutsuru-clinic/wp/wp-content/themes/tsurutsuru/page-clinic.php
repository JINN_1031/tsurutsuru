<?php get_header(); ?>

<section class="clinic_list_wrap">
  <div class="title-wrap">
    <h2><span class="title_dec-l"></span>クリニック一覧<span class="title_dec-r"></span></h2>
    <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/top/dec_clinic_title.png" alt=""></div>
    <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
  </div>
  <div class="clinic_list_container">
    <ul>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
    </ul>


    <ul>
      <li>
        <div class="clinic_list_box">
          <img src="https://placehold.jp/360x240.png" alt="">
          <a href="xxxxx" class="color-black">
            <div class="clinic_list_detail">
              <h3 class="shop-name">店舗名○○○</h3>
              <div class="address">
                <p><span class="postal-code">〒000-0000</span>〇〇県〇〇市〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇〇</p>
              </div>
              <div class="clinic-arrow"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></div>
              <div class="tel_link temp-none">
                <a href="xxxxx"><span class="icon_tel"></span>070-1234-5678<img src="<?php bloginfo('template_directory'); ?>/assets/images/common/icon_arrow-b.png" alt="" class="arrow"></a>
              </div>
            </div>
          </a>
        </div>
      </li>
    </ul>

  </div>
</section>

<?php
get_footer();