<?php
/*
Template Name: 店舗詳細
*/
?>

<?php get_header(); ?>

<div class="bg-sky">
  <section class="clinic-detail">
    <?php
      $page_data = get_the_ID();
      $page = get_post($page_data);
      $content = $page -> post_content;
    ?>
    <h1 class="clinic-title">
      <?php echo get_the_title($page_data) ?>
    </h1>

    <div class="clinic-detail_slider">
      <img src="https://placehold.jp/3d4070/ffffff/900x600.png" alt="">
    </div>

    <table>
      <tr>
        <th>住所</th>
        <td>東京都渋谷区神南前 原宿バトリアビル5F</td>
      </tr>
      <tr>
        <th>電話番号</th>
        <td>0120-772-900</td>
      </tr>
      <tr>
        <th>アクセス</th>
        <td>JR原宿駅　表参道駅より徒歩4分</td>
      </tr>
      <tr>
        <th>診療時間</th>
        <td>11:00~20:00</td>
      </tr>
      <tr>
        <th>休診日</th>
        <td>水曜日</td>
      </tr>
    </table>

    <div class="map">
      地図が表示されます。
    </div>
  </section>
</div>

<section class="president_card">
  <img src="" alt="">
  <div class="">ツルツルクリニック 銀座院　院長</div>
  <div class="">古澤 伸子（ふるさわ のぶこ）</div>
  <div class="">紹介ダミーテキスト紹介ダミーテキスト紹介ダミーテキスト紹介ダミーテキスト紹介ダミーテキスト紹介ダミーテキスト</div>
  <h2>銀座のみなさまへも、変わらぬ安心をお届けたします。</h2>
  <p>ダミーテキストダミーテキストダミーテキスト<br>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
  <a href="/confirm" class="bg-orange button_wrap">
    <span class="button_inner">
      <span>
        無料カウンセリングの予約はこちら
      </span>
    </span>
  </a>
</section>
<div class="outline-sky">
  <section class="about_sec3">
    <div class="popular-plan_wrap">
      <div class="title-wrap">
        <h2><span class="title_dec-l"></span>人気プラン<span class="title_dec-r"></span></h2>
        <div class="title-ribbon"><img src="<?php bloginfo('template_directory'); ?>/assets/images/about/dec_popular_title.png" alt=""></div>
        <div class="title-drop"><img src="<?php bloginfo('template_directory'); ?>/assets/images/common/dec_drop.png" alt=""></div>
      </div>

      <p>気軽にツルツルが目指せる！最低3回から脇だけ、うなじだけなら0,000円からご案内可能！<br>脱毛範囲と回数の組み合わせが豊富なのがツルツルクリニックの強み。<br>患者さまそれぞれの状態とご希望に合わせて最適なプランを提案します。</p>

      <div class="popular-plan_card_container">
        <ul>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">全身くまなく！まるっとツルツルPLAN</h3>
            <p>文字通り当院で照射できるすべてのパーツを脱毛します。顔やVIOも含まれていますが、もちろんご希望によって照射をしないという選択もできます。<br>また、他のサロンで脱毛を終えている部位が多い場合、割引料金でご案内できることも。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：XXXXX円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：XXXXX円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全8回：XXXXX円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：XXXXX円～</div>
              </div>
            </div>
            <span class="price_attention">※全８回・回数無制限のコースには学割（000,000円引き）、乗り換え割り（000,000円引き）のご用意も！</span>
          </li>
          <li class="popular-plan_card_list">
            <h3 class="popular-plan_h3">気になるところだけ！お手軽半身PLAN</h3>
            <p>上半身だけ、中半身だけ、下半身だけの３種類から選べます。<br>自分では手の届きにくいところ、セルフお手入れに不安があるところだけ脱毛することが可能です。<br>費用を抑えながら、ツルツルの体を手に入れたい方に。</p>
            <div class="popular-plan_price_box">
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全3回：10,000円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全5回：XXXXX円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>全8回：XXXXX円～</div>
              </div>
              <div class="popular-plan_price stripe-sky color-blue">
                <div>回数無制限：XXXXX円～</div>
              </div>
            </div>
            <span class="price_attention">※全８回・回数無制限のコースには学割（000,000円引き）、乗り換え割り（000,000円引き）のご用意も！</span>
          </li>
      </div>
  </section>
</div>

<?php
get_footer();
