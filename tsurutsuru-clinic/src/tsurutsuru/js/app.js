import $ from 'jquery';

if (process.env.NODE_ENV === 'development') {
  console.debug('development');
}

$(window).on('load', function () {

  if ($('.cal-view_container').length >= 1) {
    let holiday = false;
    let closeday = '';
    let specific = [];


    $('.cal-view_container').html('<div class="cal-set_container" data-day="' + new Date() + '" data-holiday="' + holiday + '" data-closeday="' + closeday + '" data-specific="' + specific + '"></div>');

    makeCalendar($('.cal-view_container').find('.cal-set_container'));
  }

  function makeCalendar(t) {

    //========================================
    //   初期化
    //========================================

    $(t).html('');
    let criterionDay = new Date(t.attr('data-day'));
    let holidayFlag = t.attr('data-holiday');
    let closeDay = t.attr('data-closeday');
    let closeAry = closeDay.split(',');
    let specificDay = t.attr('data-specific');
    let specificAry = specificDay.split(',');

    let WeekTbl = new Array('日', '月', '火', '水', '木', '金', '土');
    let MonthTbl = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    let setYear = criterionDay.getFullYear(); //西暦
    let setMonth = criterionDay.getMonth(); //0月~11月
    let setDay = criterionDay.getDate(); //日

    //閏年チェック
    if (((setYear % 4) == 0 && (setYear % 100) != 0) || (setYear % 400) == 0) {
      MonthTbl[1] = 29;
    }

    //========================================
    //   カレンダーの生成
    //========================================

    //上部操作ボタン挿入
    $(t).append('<div class="cal-btn_wrapper"><div class="backcal-btn">前の一週間</div><div class="nextcal-btn">次の一週間</div></div>');
    //カレンダー挿入領域の挿入
    $(t).append('<div class="cal-set_wrapper"></div>')
    //下部操作ボタン挿入
    $(t).append('<div class="cal-btn_wrapper"><div class="backcal-btn">前の一週間</div><div class="nextcal-btn">次の一週間</div></div>');

    //過去チェック
    if (setYear == new Date().getFullYear() && setMonth == new Date().getMonth() && setDay == new Date().getDate()) {
      $('.backcal-btn').addClass('disable');
    } else {
      $('.backcal-btn').removeClass('disable');
    }

    //カレンダー要素を入れる変数
    let caldom = ''

    //日にちと曜日の挿入
    caldom += '<div class="cal-set_inner cal-data_inner"><div>日時</div>';
    for (let i = 0; i < 7; i++) {
      let setCalDay = new Date(t.attr('data-day'))
      setCalDay = new Date(setCalDay.setDate(setCalDay.getDate() + i));
      caldom += '<div class="cal-day"><div><div class="cal-date">';
      caldom += (setCalDay.getMonth() + 1) + '/' + setCalDay.getDate();

      if (setCalDay.getDay() == 0) {
        caldom += '</div><div class="cal-week sunday">';
        caldom += WeekTbl[setCalDay.getDay()];
      } else if (setCalDay.getDay() == 6) {
        caldom += '</div><div class="cal-week saturday">';
        caldom += WeekTbl[setCalDay.getDay()];
      } else {
        caldom += '</div><div class="cal-week">';
        caldom += WeekTbl[setCalDay.getDay()];
      }

      caldom += '</div></div></div>';
    }
    caldom += '</div>';

    $(t).find('.cal-set_wrapper').append(caldom);

    caldom = '';
    let Timetable = new Array('9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00');
    for (let i = 0; i < 13; i++) {
      caldom += '<div class="cal-set_inner"><div>' + Timetable[i] + '</div>';
      for (let j = 0; j < 7; j++) {
        //対象の日にち
        let checkDay = new Date(setYear + '/' + t.find('.cal-data_inner > div').eq(j + 1).find('.cal-date').html());

        //祝日判定
        let HolidayFlag = false;
        if (getHoliday(checkDay) && holidayFlag == 'true') {
          HolidayFlag = true;
        }

        //定休日判定
        if ($.inArray(checkDay.getDay() + "", closeAry) >= 0) {
          HolidayFlag = true;
        }

        //休業日の判定
        $.each(specificAry, function (c, val) {
          if (t.find('.cal-data_inner > div').eq(j + 1).find('.cal-date').html() == Number(specificAry[c].slice(0, 2)) + '/' + Number(specificAry[c].slice(-2))) {
            HolidayFlag = true;
          }
        });

        if (HolidayFlag) {
          caldom += '<div class="closedday" ';
        } else {
          caldom += '<div class="activeday" ';
        }

        caldom += 'data-reserve="' + t.find('.cal-data_inner > div').eq(j + 1).find('.cal-date').html() + ' ' + Timetable[i] + '"></div>'
      }
      caldom += '</div>';
    }

    $(t).find('.cal-set_wrapper').append(caldom);

    $('[data-reserve="' + $('#date1').val() + '"]').addClass('set-reserve1');
    $('[data-reserve="' + $('#date2').val() + '"]').addClass('set-reserve2');
    $('[data-reserve="' + $('#date3').val() + '"]').addClass('set-reserve3');
  }

  //========================================
  //   祝日判定関数
  //========================================
  function getHoliday(dateobj) {
    //休日(祝日と日曜日と振替休日)を判定する関数。
    //休日ならばその名称、休日でなければ空文字を返す。
    let kyujitsu = "休日";
    let isNH = [];
    let isSun = [];
    let holidayname = [];
    let thisday, i;
    if (dateobj.getFullYear() < 2007) {
      throw new Error("2007年以上にしてください。");
    }
    for (let i = -3; i < 2; i++) {
      thisday = new Date(
        dateobj.getFullYear(),
        dateobj.getMonth(),
        dateobj.getDate() + i);
      holidayname[i] = getNationalHoliday(thisday);
      isNH[i] = (holidayname[i] != "");
      isSun[i] = thisday.getDay() === 0 ? true : false;
    }
    //国民の祝日に関する法律第三条
    //第１項　「国民の祝日」は、休日とする。
    if (isNH[0]) {
      return holidayname[0];
    }
    //第２項　「国民の祝日」が日曜日に当たるときは、
    //その日後においてその日に最も近い「国民の祝日」でない日を
    //休日とする。
    if (isSun[-1] && isNH[-1]) {
      return kyujitsu;
    }
    if (isSun[-2] && isNH[-2] && isNH[-1]) {
      return kyujitsu;
    }
    if (isSun[-3] && isNH[-3] && isNH[-2] && isNH[-1]) {
      return kyujitsu;
    }
    //第３項　その前日及び翌日が「国民の祝日」である日は、休日とする。
    if (isNH[-1] && isNH[1]) {
      return kyujitsu;
    }
    return "";
  }

  function getNationalHoliday(dateobj) {
    //祝日を判定する関数。
    //祝日ならばその名称、祝日でなければ空文字を返す。
    let nen, tsuki, hi, yobi;
    if (dateobj.getFullYear() < 2007) {
      throw new Error("2007年以上にしてください。");
    }
    nen = dateobj.getFullYear();
    tsuki = dateobj.getMonth() + 1;
    hi = dateobj.getDate();
    yobi = dateobj.getDay();

    if (tsuki == 2 && hi == 11) {
      return "建国記念の日";
    }
    if (tsuki == 4 && hi == 29) {
      return "昭和の日";
    }
    if (tsuki == 5 && hi == 3) {
      return "憲法記念日";
    }
    if (tsuki == 5 && hi == 4) {
      return "みどりの日";
    }
    if (tsuki == 5 && hi == 5) {
      return "こどもの日";
    }
    if (nen >= 2016 && tsuki == 8 && hi == 11) {
      return "山の日"; //2016-8-11から実施。
    }
    if (tsuki == 11 && hi == 3) {
      return "文化の日";
    }
    if (tsuki == 11 && hi == 23) {
      return "勤労感謝の日";
    }
    if (tsuki == 12 && hi == 23) {
      return "天皇誕生日";
    }
    if (tsuki == 1 && yobi == 1 && 7 < hi && hi < 15) {
      return "成人の日"; //1月第2月曜日;
    }
    if (tsuki == 7 && yobi == 1 && 14 < hi && hi < 22) {
      return "海の日"; //7月第3月曜日
    }
    if (tsuki == 9 && yobi == 1 && 14 < hi && hi < 22) {
      return "敬老の日"; //9月第3月曜日
    }
    if (tsuki == 10 && yobi == 1 && 7 < hi && hi < 15) {
      return "体育の日"; //10月第2月曜日
    }
    if (tsuki == 3 && hi == shunbun(nen)) {
      return "春分の日";
    }
    if (tsuki == 9 && hi == shubun(nen)) {
      return "秋分の日";
    }
    return "";
  }

  function shunbun(nen) { //春分の日
    if (nen < 1900 || nen > 2099) {
      return 0;
    }
    if (nen % 4 === 0) {
      return nen <= 1956 ? 21 : (nen <= 2088 ? 20 : 19);
    } else if (nen % 4 == 1) {
      return nen <= 1989 ? 21 : 20;
    } else if (nen % 4 == 2) {
      return nen <= 2022 ? 21 : 20;
    } else {
      return nen <= 1923 ? 22 : (nen <= 2055 ? 21 : 20);
    }
  }

  function shubun(nen) { //秋分の日
    if (nen < 1900 || nen > 2099) {
      return 0;
    }
    if (nen % 4 === 0) {
      return nen <= 2008 ? 23 : 22;
    } else if (nen % 4 == 1) {
      return nen <= 1917 ? 24 : (nen <= 2041 ? 23 : 22);
    } else if (nen % 4 == 2) {
      return nen <= 1946 ? 24 : (nen <= 2074 ? 23 : 22);
    } else {
      return nen <= 1979 ? 24 : 23;
    }
  }

  $(document).on('click', '.nextcal-btn', function () {
    let nextWeek = new Date(new Date($(this).parents('.cal-set_container').attr('data-day')).setDate(new Date($(this).parents('.cal-set_container').attr('data-day')).getDate() + 7));
    $(this).parents('.cal-set_container').attr('data-day', nextWeek);
    makeCalendar($(this).parents('.cal-set_container'));
  });

  $(document).on('click', '.backcal-btn', function () {
    let backWeek = new Date(new Date($(this).parents('.cal-set_container').attr('data-day')).setDate(new Date($(this).parents('.cal-set_container').attr('data-day')).getDate() - 7));
    $(this).parents('.cal-set_container').attr('data-day', backWeek);
    makeCalendar($(this).parents('.cal-set_container'));
  });

  $(document).on('click', '.activeday', function () {
    if ($(this).hasClass('set-reserve1') || $(this).hasClass('set-reserve2') || $(this).hasClass('set-reserve3')) {
      if ($(this).hasClass('set-reserve1')) {
        $(this).removeClass('set-reserve1')
        $('.set-reserve2').removeClass('set-reserve2').addClass('set-reserve1');
        $('#date1').val($('#date2').val());
        $('.set-reserve3').removeClass('set-reserve3').addClass('set-reserve2');
        $('#date2').val($('#date3').val());
        $('#date3').val('');
      }
      if ($(this).hasClass('set-reserve2')) {
        $(this).removeClass('set-reserve2')
        $('.set-reserve3').removeClass('set-reserve3').addClass('set-reserve2');
        $('#date2').val($('#date3').val());
        $('#date3').val('');
      }
      if ($(this).hasClass('set-reserve3')) {
        $(this).removeClass('set-reserve3');
        $('#date3').val('');
      }
    } else {
      if (!$('#date1').val()) {
        $(this).addClass('set-reserve1');
        $('#date1').val($('.set-reserve1').attr('data-reserve'));
      } else if (!$('#date2').val()) {
        $(this).addClass('set-reserve2');
        $('#date2').val($('.set-reserve2').attr('data-reserve'));
      } else if (!$('#date3').val()) {
        $(this).addClass('set-reserve3');
        $('#date3').val($('.set-reserve3').attr('data-reserve'));
      }
    }
  });
});
